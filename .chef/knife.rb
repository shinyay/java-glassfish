chef_repo = File.join(File.dirname(__FILE__), "..")
current_dir = File.dirname(__FILE__)
log_level :info
log_location STDOUT
#node_name oel67-gf
client_key "#{current_dir}/dummy.pem"
validation_client_name 'chef-validator'
validation_key "#{current_dir}/dummy.pem"
#validation_key "/etc/chef/validation.pem"
 
cookbook_path ["#{chef_repo}/cookbooks", "#{chef_repo}/site-cookbooks"]
node_path "#{chef_repo}/nodes"
role_path "#{chef_repo}/roles"
environment_path "#{chef_repo}/environments"
data_bag_path "#{chef_repo}/data_bags"
encrypted_data_bag_secret "path_to_databagkey"
knife[:berkshelf_path] = "#{chef_repo}/cookbooks"
#knife[:ssh_user] = ""
#knife[:ssh_password] = ""
knife[:ssh_port] = 22
knife[:editor] = "vim"
localmode true
 
chef_server_url 'http://127.0.0.1:8889'
syntax_check_cache_path "#{current_dir}/syntax_check_cache"
 
#puts Chef::Config.inspect
